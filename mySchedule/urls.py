from django.urls import path, re_path
from . import views

urlpatterns = [
    path('create/', views.create, name='create'),
    path('schedule/', views.schedule, name='schedule'),
    re_path('deleteschedule/(?P<delete_id>.*)/', views.delete, name="delete")
]