from django import forms

from .models import Schedule

class MyForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = [
            'nama',
            'hari',
            'tanggal',
            'jam',
            'tempat',
            'kategori',
        ]

        widgets = {
            'nama': forms.TextInput(
                attrs = {
                    'class':'form_control',
                    'placeholder':'Nama Kegiatan',
                    }
            ),
            'hari': forms.TextInput(
                attrs = {
                    'class':'form_control',
                    'placeholder':'Hari Kegiatan',
                    }
            ),
            'tanggal': forms.TextInput(
                attrs = {
                    'class':'form_control',
                    'type' : 'date',
                    }
            ),
            'jam': forms.TextInput(
                attrs = {
                    'class':'form_control',
                    'type' : 'time',
                    }
            ),
            'tempat': forms.TextInput(
                attrs = {
                    'class':'form_control',
                    'placeholder':'Tempat Kegiatan',
                    }
            ),
            'kategori': forms.TextInput(
                attrs = {
                    'class':'form_control',
                    'placeholder':'Jenis Kegiatan',
                    }
            ),

        }