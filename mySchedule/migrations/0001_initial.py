# Generated by Django 2.2.5 on 2019-10-08 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
                ('hari', models.CharField(max_length=7)),
                ('tanggal', models.DateField()),
                ('jam', models.TimeField()),
                ('tempat', models.CharField(max_length=10)),
                ('kategori', models.CharField(max_length=12)),
            ],
        ),
    ]
