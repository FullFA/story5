from django.shortcuts import render, redirect

# Create your views here.
from .forms import MyForm
from .models import Schedule

def schedule(request):
    schedules =  Schedule.objects.all()

    context = {
        'page_title':'Semua Post',
        'schedules':schedules,
    }

    return render (request, 'schedule.html', context)

def create(request):
    schedule_form = MyForm(request.POST or None)

    if request.method == 'POST': 
        if schedule_form.is_valid():
            schedule_form.save()

            return redirect('schedule')

    context = {
        'page_title' : 'Create Post',
        'post_form' : schedule_form,
    }
    return render(request, 'create.html', context)

def delete(request, delete_id):
    Schedule.objects.filter(id = delete_id).delete()

    return redirect('schedule')
