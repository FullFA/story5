from django.shortcuts import render, redirect

from mySchedule.forms import MyForm
from mySchedule.models import Schedule

def homepage(request):
    context = {
        'judul' : 'homepage'

    }
    return render(request, 'homepage.html', context)

def resume(request):
    context = {
        'judul' : 'resume'

    }
    return render(request, 'resume.html', context)

def gallery(request):
    context = {
        'judul' : 'gallery'

    }
    return render(request, 'gallery.html', context)

def mail(request):
    context = {
        'judul' : 'mail'

    }
    return render(request, 'mail.html', context)